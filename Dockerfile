FROM anapsix/alpine-java:8_jdk
MAINTAINER Matteo Giaccone <m.giaccone@ubiqueworks.com>

ENV ANDROID_TARGET_SDK="24" \
    ANDROID_BUILD_TOOLS_VERSION="24.0.2" \
    ANDROID_SDK_VERSION="24.4.1"

ENV ANDROID_HOME /opt/android-sdk-linux
ENV PATH $PATH:$ANDROID_HOME/tools
ENV PATH $PATH:$ANDROID_HOME/platform-tools

RUN apk add --no-cache curl ca-certificates bash && \
    #apk add --nocache lib32stdc++6 lib32z1 && \
    mkdir -p /opt && curl -L http://dl.google.com/android/android-sdk_r${ANDROID_SDK_VERSION}-linux.tgz | tar zxv -C /opt && \
    mkdir "$ANDROID_SDK/licenses" || true && \
    echo -e "\n8933bad161af4178b1185d1a37fbf41ea5269c55" > "$ANDROID_SDK/licenses/android-sdk-license" && \
    echo -e "\n84831b9409646a918e30573bab4c9c91346d8abd" > "$ANDROID_SDK/licenses/android-sdk-preview-license"

RUN echo "y" | android update sdk -u -a --filter tools && \
    echo "y" | android update sdk -u -a --filter platform-tools && \
    echo "y" | android update sdk -u -a --filter extra-android-m2repository && \
    echo "y" | android update sdk -u -a --filter extra-google-google_play_services && \
    echo "y" | android update sdk -u -a --filter extra-google-m2repository && \
    echo "y" | android update sdk -u -a --filter extra-google-market_apk_expansion && \
    echo "y" | android update sdk -u -a --filter extra-google-market_licensing && \
    echo "y" | android update sdk -u -a --filter extra-google-play_billing && \
    echo "y" | android update sdk -u -a --filter build-tools-${ANDROID_BUILD_TOOLS_VERSION} && \
    echo "y" | android update sdk -u -a --filter android-${ANDROID_TARGET_SDK} && \
    echo "y" | android update sdk -u -a --filter addon-google_apis-google-${ANDROID_TARGET_SDK}

